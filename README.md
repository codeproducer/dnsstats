[![pipeline status](https://gitlab.com/codeproducer/dnsstats/badges/master/pipeline.svg)](https://gitlab.com/codeproducer/dnsstats/-/commits/master)

# dnsmasq log analyzer

This project is a little commandline tool for analyzing `dnsmasq` logfiles with query informations (the `--log-queries` must be set on dnsmasq).
It logs basic information about the logfiles and the top requested domains and the top blocked domains (if dnsmasq is configured).
The tool supports as input:
* on dnsmasq logfile
* one dnsmasq gzipped logfile (when using logrotate)
* a folder containing dnsmasq logfiles (plain or gzipped)

The code is written in [Go](https://golang.org/) version _go1.16.5_ and the executable needs no runtime.

## Statistics

### Basic statistics

Shows the scanned files, the timerange and other common informations.
The numbers in `[]` are the count quantity of the context.
```
Files:              dnsmasq.log, dnsmasq.log.2, dnsmasq.log.3, dnsmasq.log.5.gz 
Time:               2021-01-26 07:05:03 - 2021-01-31 18:27:22 
Total queries:      12295 
Queries blocked:    1809 / 14.7% 
Queries cached:     3347 / 27.2% 
Clients:            2 (192.168.1.100[3037], 127.0.0.1[43])
Query types:        8 (A[1252], AAAA[1250], type=65[471], TXT[72], SRV[26], PTR[5], SOA[3], NAPTR[1])
```

### Top domains

Shows the top domains by the count of queries in descending order. The state can be
- blocked
- cached
- forward

```
===================================================================================================
| #   | Query type | Domain                              | Client IP         | Count | State      |
===================================================================================================
| 1   | A          | time-osx.g.aaplimg.com              | 192.168.1.100     | 67    | forward    |
| 2   | AAAA       | imap.gmail.com                      | 192.168.1.100     | 65    | cached     |
| 3   | A          | app-measurement.com                 | 192.168.1.100     | 63    | blocked    |
| 4   | A          | www.google.com                      | 192.168.1.100     | 18    | cached     |
```

### Top blocked domains

List of all blocked domains by the count of queries in descending order.

```

======================================================================================================================================================
| #   | Query type | Domain                               | Client IP            | Block info                                               | Count  |
======================================================================================================================================================
| 1   | AAAA       | app-measurement.com                  | fd00::3681:c4ff:fed@ | config                                                   | 611    |
| 2   | A          | tags.tiqcdn.com                      | 192.168.1.100        | /etc/dnsmasq.d/addn-hosts/blacklist_stevenblack host     | 610    |
```

### Queries by time

Contains for all scanned logfiles an row for every hour where queries exists.
It shows the count of queries for every state and the sum of all states.
It lists also all top domains[query count of all states] for this hour.

```
======================================================================================================================================================
| Hour | Total   | Blocked | Cached  | Forward | Top domains(count all states)                                                                       |
======================================================================================================================================================
| 12   | 42      | 8       | 3       | 31      | app-measurement.com[5] secure-eu.imrworldwide.com[4] ocsp-a.g.aaplimg.com[3] e14868.dsce9.akamai.c@ |
| 13   | 40      | 0       | 13      | 27      | app-measurement.com[6] startpage.com[4] firebaselogging-pa.googleapis.com[4]                        |
```

### Common remarks

- if a column does not fit the whole cell, it will be cutted and marked with a `@` character at the end.

## Usage

`./dnsstats -f mydnsmasq.log`
`./dnsstats -f samplelogs/dnsmasq*`
`./dnsstats -f samplelogs/ -l 25 -qd '.*google.' -w 230 -sl -sh -she`

The option `-f` specified 
- one logfile: `-f mydnsmasq.log`
- the folder and all files inside this folder (recursive): `-f samplelogs/` or `-f samplelogs`
- same before only with matching files the given pattern after the `/`: `-f "samplelogs/dnsmasq*"` or `-f "samplelogs/?dnsmas*.log.*"`; keep in mind on some 
Linux a `"` is needed to use the `?` or `*` option as used in the examples

as a input of the `dnsmasq` log-files. 
If the file is a `GZ` archive (and ends with `.gz`), the file is extracted to the temp folder and processed.

The option `-sl` shows the "long"-format. This means the _Query type_, _Client IP_ and the _State_ is shown in the _Basic statistics_ and the table columns.

The top- and blocked-tables shown 15 rows by default.
With `-l 100` the tables shows 100 max rows.

Use the `-q?`-option to query for a defined DNS attribute with the help of regular expressions and only shows the matched queries.
You can use the `-q?v`-option to invert the search.
Following options can be used:
* `-qd` and `-qdv` for search for the domain names (www.my.com)
* `-qc` and `-qcv` for search for the Client IP adresses (like 127.0.0.1)
* `-qt` and `-qtv` for search for the query-types (like A or PTR)

Example `-qd 'goog.*\.net' -qt 'A' -qtv` search for the domain starts with _goog_ and ends with _.net_ and 
where the query-type doesn't contain an "A".

With the option `-w <number>` you can set the width of the output from 150 - 250. Default is 150.

`./dnsstats -v` shows the version and `./dnsstats` without any parameter shows the usage.

Option `-c` enables "caching" of GZ unpacked files. These files are unpacked in the folder `/tmp/dnsstats/`.
Whitin the next run, the GZ files must not be unpacked again. But be aware, the files will be stored (with the owner
where the `dnsstats` is executing) and must be removed manually or will be deleted with the next call without this flag.
This flag is reasonable, if you run multiple queries and want to avoid excessive writing f. e. on your Raspberry SD card.

`-sh` creates additionally a HTML report named `dnsstats.html`, with a timerange of the last 7 days of your DNS activites. It's includes a line chart with
- count of all queries
- count of blocked queries
- count of cached queries

briefed in a time axis, which can zoomed.

The option `-she` creates reports with the JS-resources embedded in the HTML - the resources are about 730kB and therefor the HTML reports are larger.
Without this flag the resources are stored/shared in a local folder named `dnsstats_res` and was reused for every report (smaller reports).

The `-o <output-folder>` writes the text report into a file in the _output-folder_ (instead of stdout). The filename of the report is
`dnsstats.<timestamp>.txt` (example `dnsstats.20220415_165040.html`). In combination with `-sh` or `-she` the HTML report is also named with the timestamp
in its filename (like `dnsstats.20220415_165040.html`).
With `-r <int>` old reports will be removed, so only the newest _<int>_ files are retained.

All dates/times are UTC.

## Compile code yourself

To compile the binary use the `go build -v cmd/dnsstats/main.go` command on a configured Go environment. Go can be compiled for Windows and Linux (amd64, armhf, arm64) systems.

Example for arch _amd64_ without symbol-tables and debugging options for "production": `GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -ldflags "-s -w" -o dnsstats_linux_amd64 cmd/dnsstats/main.go`

Example for building for development `go build -tags=debug cmd/dnsstats/main.go` where `main` binary is created.

If you want more debugging info while running the `dnsstats` use the flag `-tags=debug` for enabling debugging in the binary.

## Use precompiled releases

You can also use the compiled [releases](https://gitlab.com/codeproducer/dnsstats/-/releases) for several linux systems.
Download the file, change the execution permission `chmod u+x dnsstats*` and start it with a leading `./dnsstats_v0.20_linux_arm64`.
