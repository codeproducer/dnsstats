package main

import (
	"dnsstats/internal/cleanup"
	"dnsstats/internal/config"
	"dnsstats/internal/debug"
	"dnsstats/internal/parser"
	"dnsstats/internal/presentation/presascii"
	"dnsstats/internal/presentation/preshtml"
	"dnsstats/internal/version"
	"flag"
	"fmt"
	"os"
	"time"
)

func init() {
	debug.Init()
}

func main() {
	// set and parse the parameters/flags (result of flag. is a pointer)
	filename := flag.String("f", "", "a filename, a gz filename or a folder")
	limit := flag.Int("l", 15, "limit for tops (1 - 150)")
	width := flag.Int("w", 150, "width of output in characters (150 - 250)")
	qDomain := flag.String("qd", "", "regular query for the domain")
	qDomainV := flag.Bool("qdv", false, "invert the query for the domain")
	qClientIp := flag.String("qc", "", "regular query for the client IP")
	qClientIpV := flag.Bool("qcv", false, "invert the query for the client IP")
	qType := flag.String("qt", "", "regular query for the type")
	qTypeV := flag.Bool("qtv", false, "invert the query for the type")
	sLong := flag.Bool("sl", false, "show query-type, client-ip and state and block-info")
	sHtml := flag.Bool("sh", false, "create HTML report \"dnsstats.html\"")
	sHtmlE := flag.Bool("she", false, "create HTML report with embedded resources")
	outFolder := flag.String("o", "", "folder for the output")
	cacheUnzip := flag.Bool("c", false, "if set, unzipped files will not be deleted (under /tmp/dnsstats) and reused in the next call")
	rotate := flag.Int("r", 0, "rotate and keep the last n reports; only valid with -o")
	showVersion := flag.Bool("v", false, "version information")
	flag.Parse()

	if *showVersion {
		version.PrintVersionInfo()
		os.Exit(0)
	}

	creationTime := time.Now()

	// check parameters and set the layout
	layout := config.HandleFlags(filename, limit, width, cacheUnzip, sLong)
	outText, outChart := config.HandleOutputFlags(creationTime, sHtml, sHtmlE, outFolder, rotate)

	// first parse the file or file in folder to the lowest DnsEntry level
	queryParam := parser.MakeParameter(*qDomain, *qDomainV, *qClientIp, *qClientIpV, *qType, *qTypeV)

	parserParam := parser.ParserParameter{Query: queryParam, Id: layout.ParserId}
	report := parser.Parse(*filename, parserParam, creationTime)

	if report.HasEntries() {
		presascii.PrintBasicStats(outText, report, layout.PreColumns)
		fmt.Fprintf(outText, "\n")

		// compress to top level and show top-table
		topEntries := parser.Compress(report.DnsEntries, layout.TopDomainsId)
		presascii.PrintTops(outText, topEntries, *limit)

		// compress to time level, merge results and show timed-table
		fmt.Fprintf(outText, "\n")
		timeStateEntries := parser.Compress(report.DnsEntries, layout.LayoutTimed.TimeStateId)   // compress to time and state(s)
		timeDomainEntries := parser.Compress(report.DnsEntries, layout.LayoutTimed.TimeDomainId) // compress to time and domains
		presascii.PrintTimed(outText, timeStateEntries, timeDomainEntries)

		// generate a html file/report
		if *sHtml || *sHtmlE {
			dateHourEntries := parser.Compress(report.DnsEntries, parser.DateHourStateId)
			preshtml.CreateHtmlChart(outChart, report.CopyReport(dateHourEntries), *sHtmlE)
		}

		// remove old reports
		if *outFolder != "" && *rotate > 0 {
			cleanup.Rotate(*outFolder, *rotate, cleanup.TextPattern)
			if *sHtml || *sHtmlE {
				cleanup.Rotate(*outFolder, *rotate, cleanup.ChartPattern)
			}
		}
	} else {
		fmt.Printf("No dnsmasq log entries found, nothing to shown.\n")
	}
}
