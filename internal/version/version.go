package version

import (
	"fmt"
	"runtime"
)

var Version = "dev"
var BuildTime = "unknown"

func PrintVersionInfo() {
	fmt.Printf("dnsstats version %s build with %s for %s/%s at %s\n", Version, runtime.Version(), runtime.GOOS, runtime.GOARCH, BuildTime)
	fmt.Printf("Informations about see https://gitlab.com/codeproducer/dnsstats\n")
}
