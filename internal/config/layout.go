// layout configuration for the presentation; so what columns shall be displayed, which id's are needed for parsing and compression

package config

import (
	"dnsstats/internal/parser"
	"dnsstats/internal/presentation/presascii"
)

// defines the layout of the output
type Layout struct {
	// which columns are shown
	PreColumns presascii.PreColumnLayout

	// used parser for the parsing of the log-files
	ParserId parser.DnsEntryId

	// compressor for the top (blocked) domains
	TopDomainsId parser.DnsEntryId
	// compressor for the timed-table
	LayoutTimed LayoutTimed
}

// compressor for the timed-table
type LayoutTimed struct {
	// compress the dns entries to hour & state
	TimeStateId parser.DnsEntryId
	// compress the dns entries to hour & domain
	TimeDomainId parser.DnsEntryId
}

// build upon the input the shown columns, used file parserId and the compressId's
func BuildLayout(sLong bool) Layout {
	// layout of which colums are shown/present
	preCol := presascii.PreColumnLayout{Typ: sLong, ClientIp: sLong, State: sLong}
	// the compressor for the timed-table is currently always the same
	timed := LayoutTimed{TimeStateId: parser.HourStateId, TimeDomainId: parser.HourDomainId}

	var parserId parser.DnsEntryId
	var topDomainsId parser.DnsEntryId
	if sLong {
		parserId = parser.TimeTypeDomainClientStateId
		topDomainsId = parser.TypeDomainClientStateId
	} else {
		parserId = parser.TimeDomainStateId
		topDomainsId = parser.DomainId
	}

	return Layout{
		PreColumns:   preCol,
		ParserId:     parserId,
		TopDomainsId: topDomainsId,
		LayoutTimed:  timed}
}
