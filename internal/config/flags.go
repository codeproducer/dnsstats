package config

import (
	"dnsstats/internal/parser"
	"dnsstats/internal/presentation"
	"dnsstats/internal/presentation/presascii"
	"flag"
	"fmt"
	"os"
	"time"
)

const FilePrefix = "dnsstats"
const FileSuffixText = ".txt"
const FileSuffixChart = ".html"

// check if the required cmd-line parameters are set
// set global configuration to the submodules
func HandleFlags(filename *string, limit *int, width *int, cacheUnzip *bool, sLong *bool) Layout {
	if *filename == "" {
		fmt.Printf("The parameter -f is required. See -h for help!\n")
		flag.PrintDefaults()
		os.Exit(1)
	}

	if *limit <= 0 || *limit > 150 {
		fmt.Printf("The parameter -l must be between than 1 and 150. See -h for help!\n")
		flag.PrintDefaults()
		os.Exit(1)
	}

	if *width < presascii.DefaultWidth || *width > 250 {
		fmt.Printf("The parameter -w must be between than 150 and 250. See -h for help!\n")
		flag.PrintDefaults()
		os.Exit(1)
	}

	layout := BuildLayout(*sLong)

	// 150 is the default width; set here the additional
	presascii.UpdatePresentationConfiguration(*width-presascii.DefaultWidth, layout.PreColumns)

	// set unzip cache strategy
	parser.InitGzip(*cacheUnzip)

	return layout
}

// check the outFolder flag (if a valid directory) and
// return the test-report filename and the HTLM-chart name
func HandleOutputFlags(creationTime time.Time, sHtml *bool, sHtmlE *bool, outFolder *string, rotate *int) (*os.File, *os.File) {
	outText := os.Stdout
	var outChart *os.File

	// if a outFolder is definied, use this for the reports
	if *outFolder != "" {
		info, err := os.Stat(*outFolder)

		if err != nil && os.IsNotExist(err) {
			fmt.Printf("The given output-folder %s do not exists! Error is: %s\n", *outFolder, err.Error())
			os.Exit(1)
		} else if !info.IsDir() {
			fmt.Printf("The given output-folder %s is no folder!\n", *outFolder)
			os.Exit(1)
		} else {
			name := *outFolder + string(os.PathSeparator) + FilePrefix + creationTime.Format(presentation.TimeFormatFilePrefix) + FileSuffixText
			var createErr error
			outText, createErr = os.Create(name)
			if createErr != nil {
				fmt.Printf("Can not create report on output-folder %s! Error is: %s\n", *outFolder, createErr.Error())
				os.Exit(1)
			}
		}
	} else {
		if *rotate > 0 {
			fmt.Printf("-r without -o is not allowed\n")
			flag.PrintDefaults()
			os.Exit(1)
		}
	}

	if *sHtml && *sHtmlE {
		fmt.Printf("Please choose flag sh or she, not both\n")
		flag.PrintDefaults()
		os.Exit(1)
	}

	if *sHtml || *sHtmlE {
		var name string
		if *outFolder != "" {
			name = *outFolder + string(os.PathSeparator) + FilePrefix + creationTime.Format(presentation.TimeFormatFilePrefix) + FileSuffixChart
		} else {
			name = FilePrefix + FileSuffixChart
		}
		var createErr error
		outChart, createErr = os.Create(name)
		if createErr != nil {
			fmt.Printf("Can not create report on output-folder %s! Error is: %s\n", *outFolder, createErr.Error())
			os.Exit(1)
		}
	} else {
		outChart = nil
	}

	return outText, outChart
}
