// offline/local renderer without any resource loading from outside
package preshtml

import (
	_ "embed"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
)

// ########################
// use one header-template without the `<script src="https://go-echarts.github.io/go-echarts-assets/assets/xxx.js"></script>"`
// and without load resourcen when the file is shown in the browser
//
// all resourcen was download from the archived URL and empty lines and remarks are removed
// ########################

// archived from https://go-echarts.github.io/go-echarts-assets/assets/echarts.min.js
//go:embed echarts-asserts/echarts.min.js
var echartsMinJs string

// archived from https://go-echarts.github.io/go-echarts-assets/assets/themes/westeros.js
//go:embed echarts-asserts/westeros.js
var westerosJs string

// copy from go-echarts/templates/header.go
var headerTpl = `
{{ define "header" }}
<head>
   <meta charset="utf-8">
   <title>dnsstats report</title>
   #echarts.min.js#
   #westeros.js#
{{- range .CSSAssets.Values }}
   <link href="{{ . }}" rel="stylesheet">
{{- end }}
</head>
{{ end }}
`
const resourceFolder = "dnsstats_res"

// the header where the JS resources are stored as local files and reference to it
// returns the header for the created chart page
func localHeader(outChart *os.File) *string {
	// create foldern and resource files
	createLocalFiles(outChart)

	var h string
	// replace the constant marker of the header with the embed constants
	h = strings.Replace(headerTpl, "#echarts.min.js#", "<script src=\"" + resourceFolder + "/dnsstats_report_js.js\"></script>", 1)
	h = strings.Replace(h, "#westeros.js#", "<script src=\"" + resourceFolder + "/dnsstats_report_css.js\"></script>", 1)

	return &h
}

func createLocalFiles(outChart *os.File) {
	// extracts the base-folder of the html-report-file to check resources-folder
	outFolder := filepath.Join(filepath.Dir(outChart.Name()), resourceFolder)

	// create local resoures
    err := os.Mkdir(outFolder, 0755)
    if err != nil {
		log.Printf("Resource folder %s already exists, not needed to create of folder and files.", outFolder)
    } else {
		log.Printf("Resource folder %s created and now creating resource files.", outFolder)
		createFile(outFolder, &echartsMinJs, "dnsstats_report_js.js")
		createFile(outFolder, &westerosJs, "dnsstats_report_css.js")
	}
}

func createFile(outFolder string, content *string, name string) {
	f, err := os.Create(filepath.Join(outFolder + "/" + name))

    if err != nil {
        fmt.Printf("Can't create report resource file: %s", err)
		os.Exit(1)
    }
    defer f.Close()

	_, err2 := f.WriteString(*content)

    if err2 != nil {
        fmt.Printf("Can't write report resource file: %s", err)
		os.Exit(1)
    }
}

// the header where the JS resources are included
// returns the header for the created chart page
func embedHeader() *string {
	var h string
	// replace the constant marker of the header with the embed constants
	h = strings.Replace(headerTpl, "#echarts.min.js#", "<script>"+echartsMinJs+"</script>", 1)
	h = strings.Replace(h, "#westeros.js#", "<script>"+westerosJs+"</script>", 1)

	return &h
}
