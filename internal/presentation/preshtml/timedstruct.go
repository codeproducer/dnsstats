// structure for the html line bases tables
package preshtml

import (
	"dnsstats/internal/parser"
	"time"

	"github.com/go-echarts/go-echarts/v2/opts"
)

// ################################### one entry

// one entry (node) for the line table
type LineEntry struct {
	month			string		// "month"
	day 			string 		// "day"
	hour 			string 		// "02"

	countAll     	int      	// count of all quiries
	countBlocked    int      	// count of blocked quiries
	countCached	    int      	// count of cached quiries
}

// id for a entry which used for "compression"/key of the map
func (e *LineEntry) id() string {
	return e.month + e.day + e.hour
}

// table column description of an entry
func (e LineEntry) desc() string {
	return e.month + "-" + e.day + "\n" + e.hour + ":00"
}

// ################################### all entry container

// all entries (nodes) for a table
type LineEntries struct {
	entries 		map[string]LineEntry
	days 			int			// number of days
	sortedKeys		[]string	// sorted list of map keys
	timeFrom   		time.Time
	timeTo     		time.Time
	creationTime	time.Time
}

// description of a table X axis
func (e LineEntries) axisDesc() []string {
	r := make([]string, len(e.entries))

	for i, k := range e.sortedKeys {
		r[i] = e.entries[k].desc()
	}

	return r
}

// create the line-data (e-charts) from the entries 
func (e LineEntries) generateLineItems(blocked bool, cached bool) []opts.LineData {
    r := make([]opts.LineData, len(e.entries))

	for i, k := range e.sortedKeys {
		if blocked {
			r[i] = opts.LineData{Value: e.entries[k].countBlocked}
		} else if cached {
			r[i] = opts.LineData{Value: e.entries[k].countCached}
		} else {
			r[i] = opts.LineData{Value: e.entries[k].countAll}
		}
	}

    return r
}

// add the previous compressed (raw) DNS entries to the line table structure
func (le *LineEntries) add(dnsEntries *[]parser.DnsEntry) {
	for _, e := range *dnsEntries {
		// get the id of the DNS entry
		id := e.Timestamp.Format("010215")
		// all line entries shall be present, so only add the "count" from the raw DNS to the line-entries
		if v, ok := le.entries[id]; ok {
			v.countAll = v.countAll + e.Count

			if e.IsBlocked() {
				v.countBlocked+= e.Count
			} else if e.IsCached() {
				v.countCached+= e.Count
			}
			le.entries[id] = v
		}
	}
}