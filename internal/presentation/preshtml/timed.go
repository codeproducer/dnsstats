// html report with line chart
package preshtml

import (
	"dnsstats/internal/parser"
	"dnsstats/internal/presentation"
	"log"
	"os"
	"sort"
	"time"

	"github.com/go-echarts/go-echarts/v2/charts"
	"github.com/go-echarts/go-echarts/v2/components"
	"github.com/go-echarts/go-echarts/v2/opts"
	"github.com/go-echarts/go-echarts/v2/types"
)

// max days are shown; we the report is larger than these days, only the last X days are shown
const maxDays = 7

// calculate the start time and the days which are shown
// the range can be reduced to the `maxDays`
func calcReportDays(report parser.DnsReport) (time.Time, int) {
	startTime := report.TimeFrom
	diff := report.TimeTo.Sub(startTime)
	days := int(diff.Hours() / 24.0 + 0.5) // add 0.5 for rounding

	if days == 0 {
		days = 1
	} else if days > maxDays {
		// if days are greater than 7, reduce the range
		log.Printf("report is limited to %d days", days)

		startTime = report.TimeTo.Add(-(time.Hour * 24 * maxDays))
		days = maxDays
	}
	log.Printf("report started %v from with %d days", startTime, days)

	return startTime, days
}

// create based on the timerange a LineEntries structure (a map) which can be filled with "DNS counts"
// and used for the axis and the values of the line chart
func createEntries(report parser.DnsReport) LineEntries {
	// determine start-date and the days
	startTime, days := calcReportDays(report)

	// build empty structure map
	e := make(map[string]LineEntry)
	
	d := startTime
	for {
		entry := LineEntry{month: d.Format("01"), day: d.Format("02"), hour: d.Format("15")}
		e[entry.id()] = entry

		d = d.Add(time.Hour)
		if !d.Before(report.TimeTo) {
			entry := LineEntry{month: d.Format("01"), day: d.Format("02"), hour: d.Format("15")}
			e[entry.id()] = entry
			break
		} 
	}

	// create sorted keys
	keys := make([]string, 0)
    for k := range e {
        keys = append(keys, k)
    }
    sort.Strings(keys)

	// create entry wrapper
	le := LineEntries{entries: e, days: days, sortedKeys: keys, timeFrom: startTime, timeTo: report.TimeTo, creationTime: report.CreationTime}
	le.add(&report.DnsEntries)

	return le
}

// creates a html report/page on the local file system
func CreateHtmlChart(outChart *os.File, report parser.DnsReport, embedHeader bool) {
	page := components.NewPage()
	page.Renderer = newOfflinePageRender(outChart, embedHeader, page, page.Validate)

	lineEntries := createEntries(report)
	page.AddCharts(createLineChart(lineEntries))
    _ = page.Render(outChart)
}

// a line chart
func createLineChart(lineEntries LineEntries) *charts.Line {
	// create a new line instance
    line := charts.NewLine()

    // set some global options like Title/Legend/ToolTip or anything else
	line.SetGlobalOptions(
		charts.WithInitializationOpts(opts.Initialization{
			PageTitle: "dnsstats report", // not working, set it alos in the offlinerender
			Theme: types.ThemeWesteros, 
			Width: "1800px", Height: "500px",
		}),
		charts.WithTitleOpts(opts.Title{
			Top: "0%",
			Left: "5%",
			Title:    "DNS queries from " + lineEntries.timeFrom.Format(presentation.TimeFormat) + " to " + lineEntries.timeTo.Format(presentation.TimeFormat),
			Subtitle: "Created at " + lineEntries.creationTime.Format(presentation.TimeFormat) + " on host " + presentation.Hostname(),
		}),
		charts.WithLegendOpts(opts.Legend{Show: true, Top: "5%", Right: "10%"}),
		charts.WithXAxisOpts(opts.XAxis{
			SplitNumber: 20,
		}),
		charts.WithYAxisOpts(opts.YAxis{
			Scale: false,
		}),
		charts.WithDataZoomOpts(opts.DataZoom{
			Start:      0,
			End:        100,
			XAxisIndex: []int{0},
		}),
	)

	// add the X and the line series
	line.SetXAxis(lineEntries.axisDesc()).
		AddSeries("Total", lineEntries.generateLineItems(false, false), charts.WithLineStyleOpts(opts.LineStyle{Width: 3})).
		AddSeries("Blocked", lineEntries.generateLineItems(true, false)).
		AddSeries("Cached", lineEntries.generateLineItems(false, true)).
		SetSeriesOptions(
			charts.WithLineChartOpts(opts.LineChart{Smooth: true}), 
			charts.WithLabelOpts(opts.Label{Show: true, Formatter: "{c}"}),
	)

	return line
}
