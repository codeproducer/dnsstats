// offline/local renderer without any resource loading from outside
package preshtml

import (
	"bytes"
	_ "embed"
	"io"
	"os"
	"regexp"

	"github.com/go-echarts/go-echarts/v2/render"
	tpls "github.com/go-echarts/go-echarts/v2/templates"
)

// ########################
// Renderer for a **page**
// copied from: https://github.com/go-echarts/go-echarts/blob/fc14e0f49b77002aeb54be6a0dbc3bf330ade489/render/engine.go
// to use my own renderer which uses my custom header without the after-loading JS resources
// ########################

type offlinePageRender struct {
	header *string
	c      interface{}
	before []func()
}

func newOfflinePageRender(outChart *os.File, useEmbedHeader bool, c interface{}, before ...func()) render.Renderer {
	// determine the wanted header
	var header *string
	if useEmbedHeader {
		header = embedHeader() // embedded header
	} else {
		header = localHeader(outChart) // local files with ref
	}

	return &offlinePageRender{header: header, c: c, before: before}
}

func (r *offlinePageRender) Render(w io.Writer) error {
	for _, fn := range r.before {
		fn()
	}

	contents := []string{*r.header, tpls.BaseTpl, tpls.PageTpl}
	tpl := render.MustTemplate("page", contents)

	var buf bytes.Buffer
	if err := tpl.ExecuteTemplate(&buf, "page", r.c); err != nil {
		return err
	}

	pat := regexp.MustCompile(`(__f__")|("__f__)|(__f__)`)
	content := pat.ReplaceAll(buf.Bytes(), []byte(""))

	_, err := w.Write(content)
	return err
}
