package presentation

import (
	"bytes"
	"os"
)

const TimeFormat = "2006-01-02 15:04:05"
const TimeFormatFilePrefix = ".20060102_150405"

// fullfill a string to the length with the filler
// if the string already is longer then length a @ is set on the last char
func StringFull(s string, length int, filler string) string {
	var result string
	sl := len(s)

	// if existing string lower than the size, fill up with blanks to the size
	if sl < length {
		var b bytes.Buffer
		b.WriteString(s)

		for i := 0; i < length-sl; i++ {
			b.WriteString(filler)
		}

		result = b.String()
	} else if sl > length {
		result = s[0:length-1] + "@"
	} else {
		result = s
	}

	return result
}

func Hostname() string {
	hostname, err := os.Hostname()
	if err != nil {
		return "unknown"
	} else {
		return hostname
	}
}
