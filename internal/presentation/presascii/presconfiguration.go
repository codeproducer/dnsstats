package presascii

import "log"

// constants and configuration for the presentation package

const DefaultWidth = 150 // default width in chars
var tableExtendWidth int // value from 0 - 100 to extend the DefaultWidth

const (
	// available column definition for top-tables
	columnTopIdx       int = 0
	columnTopQueryType     = 1
	columnTopDomain        = 2
	columnTopClientIp      = 3
	columnTopCount         = 4
	columnTopState         = 5
	columnTopBlockInfo     = 6

	// available column definition for timed-tables
	columnTimedHour         = 7
	columnTimedStateTotal   = 8
	columnTimedStateBlocked = 9
	columnTimedStateCached  = 10
	columnTimedStateForward = 11
	columnTimedDomains      = 12
)

// defines the {columns and size}
// go do not support const for arrays; use pointers, so not on every function call a copy must create
var topTableDomains = &[]columnFormat{}
var topTableBlock = &[]columnFormat{}

var timedTable = &[]columnFormat{}

// presentation colums to show/hide
type PreColumnLayout struct {
	Typ      bool // is the type column shown
	ClientIp bool // is the client shown
	State    bool // is state and block-info shown
}

// updates the internal table structure for the width extension and the shown columns
func UpdatePresentationConfiguration(tabExtendWidth int, columns PreColumnLayout) {
	tableExtendWidth = tabExtendWidth

	initTopTableDomains(tabExtendWidth, columns)
	initTopTableBlock(tabExtendWidth, columns)

	timedTable = &[]columnFormat{
		{columnTimedHour, 4, 0, "Hour"},
		{columnTimedStateTotal, 8, 0, "Total"},
		{columnTimedStateBlocked, 7, 0, "Blocked"},
		{columnTimedStateCached, 7, 0, "Cached"},
		{columnTimedStateForward, 7, 0, "Forward"},
		{columnTimedDomains, 98, 100, "Top domains(count all states)"}}
	updateColumnFormat(tableExtendWidth, timedTable)
}

// init the table columns
// if one column is not shown/present, the unused space (size + extend) is used by another (f.e. Domain) column
func initTopTableDomains(tabExtendWidth int, columns PreColumnLayout) {
	var col []columnFormat
	col = append(col, columnFormat{columnTopIdx, 3, 0, "#"})

	if columns.Typ {
		col = append(col, columnFormat{columnTopQueryType, 10, 0, "Query type"})
	}

	col = append(col, columnFormat{columnTopDomain, 60, 70, "Domain"})

	if !columns.Typ { // the domain now exists, we can update it because the type is not present
		updateColumnFormatSizeExtend(&col, columnTopDomain, 10, 0)
	}

	if columns.ClientIp {
		col = append(col, columnFormat{columnTopClientIp, 42, 30, "Client IP"})
	} else {
		updateColumnFormatSizeExtend(&col, columnTopDomain, 42, 30)
	}

	col = append(col, columnFormat{columnTopCount, 6, 0, "Count"})

	if columns.State {
		col = append(col, columnFormat{columnTopState, 10, 0, "State"})
	} else {
		updateColumnFormatSizeExtend(&col, columnTopDomain, 10, 0)
	}

	topTableDomains = &col

	updateColumnFormat(tableExtendWidth, topTableDomains)
}

// init the table columns
// if one column is not shown/present, the unused space (size + extend) is used by another (f.e. Domain) column
func initTopTableBlock(tabExtendWidth int, columns PreColumnLayout) {
	var col []columnFormat
	col = append(col, columnFormat{columnTopIdx, 3, 0, "#"})

	if columns.Typ {
		col = append(col, columnFormat{columnTopQueryType, 10, 0, "Query type"})
	}

	col = append(col, columnFormat{columnTopDomain, 36, 36, "Domain"})

	if !columns.Typ { // the domain now exists, we can update it because the type is not present
		updateColumnFormatSizeExtend(&col, columnTopDomain, 10, 0)
	}

	if columns.ClientIp {
		col = append(col, columnFormat{columnTopClientIp, 20, 28, "Client IP"})
	} else {
		updateColumnFormatSizeExtend(&col, columnTopDomain, 20, 28)
	}

	if columns.State {
		col = append(col, columnFormat{columnTopBlockInfo, 56, 36, "Block info"})
	} else {
		updateColumnFormatSizeExtend(&col, columnTopDomain, 56, 36)
	}

	col = append(col, columnFormat{columnTopCount, 6, 0, "Count"})

	topTableBlock = &col

	updateColumnFormat(tableExtendWidth, topTableBlock)
}

// is used if one column is hidden, the unused size+extend to transfer to another column
// i have consider about to implement this behaviour without manual setting only with calculation,
// but it's poor but it works ;-)
func updateColumnFormatSizeExtend(columns *[]columnFormat, column int, size int, extend int) {
	var col *columnFormat
	for i := range *columns {
		col = &(*columns)[i]

		if col.num == column {
			(*columns)[i].size += size + 3 // the 3 are the border
			(*columns)[i].extend += extend
			log.Printf("increase column %s size to %d and extend to %d", col.head, col.size, col.extend)
		}
	}

}

// the ColumnFormat has a default width (=size) and a extend (=percent value)
// in this func the percent value of the extended value is added to the size
func updateColumnFormat(tableExtendWidth int, columns *[]columnFormat) {
	var col *columnFormat
	for i := range *columns {
		col = &(*columns)[i]
		// is this column is extendable
		if col.extend > 0 {
			orgSize := col.size
			// percent value of the extend for this column
			extend := tableExtendWidth * col.extend / 100
			(*columns)[i].size += extend
			log.Printf("extend colum %s by %d percent/%d from %d to %d", col.head, col.extend, extend, orgSize, (*columns)[i].size)
		}
	}
}
