package presascii

import (
	"dnsstats/internal/parser"
	"fmt"
	"os"
	"sort"
	"strconv"
)

// sort DnsEntry's by the count descending
func sortByCount(entries []parser.DnsEntry) []parser.DnsEntry {
	sort.Slice(entries, func(i, j int) bool {
		return entries[i].Count > entries[j].Count
	})

	return entries
}

// wrapper type for one row of the tops table (includes a DnsEntry)
type topsRow struct {
	idx   int
	entry *parser.DnsEntry
}

// retrieves the column value for a row
func (row topsRow) value(column int) string {
	var txt string

	e := row.entry

	switch column {
	case columnTopIdx:
		txt = strconv.Itoa(row.idx)
	case columnTopQueryType:
		txt = e.QueryType
	case columnTopDomain:
		txt = e.Domain
	case columnTopClientIp:
		txt = e.ClientIp
	case columnTopCount:
		txt = strconv.Itoa(e.Count)
	case columnTopState:
		txt = e.State.ToString()
	case columnTopBlockInfo:
		txt = e.BlockInfo
	}
	return txt
}

// print top statistics of a report and limit the top entries
func PrintTops(out *os.File, dnsEntries []parser.DnsEntry, limit int) {
	sorted := sortByCount(dnsEntries)

	// Print TopDomains
	fmt.Fprintf(out, "Top domains:\n")
	printDnsEntryTableHeader(out, topTableDomains)

	for i, v := range sorted {
		printDnsEntryTableRow(out, topsRow{i + 1, &v}, topTableDomains)

		if i == limit-1 {
			break
		}
	}

	// Print Top Blocks
	fmt.Fprintf(out, "\nTop blocked domains:\n")
	printDnsEntryTableHeader(out, topTableBlock)

	// use a separate counter because we only interest on "blocked"
	i := 0
	for _, v := range sorted {
		if v.IsBlocked() {
			printDnsEntryTableRow(out, topsRow{i + 1, &v}, topTableBlock)

			if i == limit-1 {
				break
			}
			i++
		}
	}
}
