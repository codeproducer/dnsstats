package presascii

import (
	"dnsstats/internal/parser"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

// maximal merged top domains are added to the result timedRow
// the domains are joined to a string and can be cutted by the column size
const maxTopDomains = 8

// sort timedRow's by the id(=time) asc
func sortById(entries []timedRow) []timedRow {
	sort.Slice(entries, func(i, j int) bool {
		return entries[i].id < entries[j].id
	})

	return entries
}

// sort DnsEntry's by the hour(asc) & count(desc) descending
func sortByTimeAndCount(entries []parser.DnsEntry) []parser.DnsEntry {
	sort.Slice(entries, func(i, j int) bool {
		if entries[i].Hour() < entries[j].Hour() {
			return true
		}
		if entries[i].Hour() > entries[j].Hour() {
			return false
		}
		return entries[i].Count > entries[j].Count
	})

	return entries
}

// wrapper type for one row of the time table
type timedRow struct {
	id         string            // the HH/hour
	stateCount [3]int            // count of the states
	domains    []parser.DnsEntry // used for topDomains
}

// retrieves the column value for a row
func (row timedRow) value(column int) string {
	var i int

	switch column {
	case columnTimedHour:
		return row.id
	case columnTimedStateTotal:
		i = row.stateCount[parser.Blocked] + row.stateCount[parser.Cached] + row.stateCount[parser.Forward]
	case columnTimedStateBlocked:
		i = row.stateCount[parser.Blocked]
	case columnTimedStateCached:
		i = row.stateCount[parser.Cached]
	case columnTimedStateForward:
		i = row.stateCount[parser.Forward]
	case columnTimedDomains: // the top domains
		if len(row.domains) > 0 {
			var txt strings.Builder
			for i = 0; i < len(row.domains) && i < maxTopDomains; i++ {
				if i > 0 {
					txt.WriteString(" ")
				}
				txt.WriteString(fmt.Sprintf("%s[%d]", row.domains[i].Domain, row.domains[i].Count))
			}
			return txt.String()
		} else {
			return ""
		}
	}

	return strconv.Itoa(i)
}

// print timed statistics
// the first parameter is hour based entries with the states (means 21 o'clock has the entries for forward, blocked...)
// the second is hour based entries with the domains (means 21 o'clock has google.com, startpage...)
// both maps will be merged to one hour based "together"
func PrintTimed(out *os.File, dnsEntriesState []parser.DnsEntry, dnsEntriesDomain []parser.DnsEntry) {
	// Print TopDomains
	fmt.Fprintf(out, "Queries by time:\n")

	// sort by id=hour
	sorted := sortById(compress(dnsEntriesState))

	// add top tomains to exiting timedRows / merge together
	mergeTopDomains(&sorted, dnsEntriesDomain)

	// print header
	printDnsEntryTableHeader(out, timedTable)

	// print rows
	for _, v := range sorted {
		printDnsEntryTableRow(out, v, timedTable)
	}
}

// compress the pre-prepared hour_stateCountto hour/timeRow-base with stateCountsummary
func compress(dnsEntries []parser.DnsEntry) []timedRow {
	// now compress the entries to only the first id = HH/hours
	resultMap := make(map[string]timedRow)
	for _, e := range dnsEntries {
		// split the id fields (delimiter is _)
		ids := strings.Split(e.Id, string(parser.Deli))
		// the first id field is the HH/hour
		id := ids[0]

		if v, ok := resultMap[id]; ok {
			// update the count of an existing hour enty
			v.stateCount[e.State] += e.Count
			resultMap[id] = v
		} else {
			// create/add new hour entry
			v := timedRow{id: id}
			v.stateCount[e.State] += e.Count
			resultMap[id] = v
		}
	}

	// flat map to array
	result := []timedRow{}
	for _, v := range resultMap {
		result = append(result, v)
	}

	log.Printf("compress from %d DnsEntrys to %d timedRow", len(dnsEntries), len(result))

	return result
}

// merges the top domains to the timedRow
func mergeTopDomains(timeRows *[]timedRow, dnsEntriesDomain []parser.DnsEntry) {
	// sort the domains by hour(asc) & count(desc)
	sortedDomains := sortByTimeAndCount(dnsEntriesDomain)

	// iterate over all timedRows and add the 5 first domains
	var idx int
	var v *timedRow
	for i := range *timeRows {
		v = &(*timeRows)[i]

		// we search the first index starts with the hour from timedRow
		idx = sort.Search(len(sortedDomains), func(i int) bool {
			return string(sortedDomains[i].Id) >= v.id
		})

		// use [idx...idx+5] domains
		max := idx + maxTopDomains
		for ; idx < max && idx < len(sortedDomains); idx++ {
			// is the index the same hour?
			if strings.HasPrefix(sortedDomains[idx].Id, v.id) {
				v.domains = append(v.domains, sortedDomains[idx])
			}
		}
	}
}
