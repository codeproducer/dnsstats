package presascii

import (
	"dnsstats/internal/presentation"
	"fmt"
	"os"
)

const headerChar = "="

// format of a table column
type columnFormat struct {
	// number of the column
	num int
	// size of content in chars; this do not include borders and padding only the content
	size int
	// percent of extends; all colums should summarize to 100 or 0 for no extends
	extend int
	// head text of the column
	head string
}

// a table row must implement this interface
// where the Value returns the value for a particular column-number
type rowValueHolder interface {
	value(column int) string
}

// print the header based on the definded columns
func printDnsEntryTableHeader(out *os.File, columns *[]columnFormat) {
	var tableSize = calcSize(columns)

	// header border bottom
	fmt.Fprintln(out, presentation.StringFull("", tableSize, headerChar))

	// header columns
	fmt.Fprintf(out, "| ")

	for i, v := range *columns {
		if i > 0 {
			fmt.Fprintf(out, " | ")
		}
		fmt.Fprintf(out, "%s", presentation.StringFull(v.head, v.size, " "))
	}
	fmt.Fprintf(out, " |\n")

	// header border bottom
	fmt.Fprintln(out, presentation.StringFull("", tableSize, headerChar))
}

func printDnsEntryTableRow(out *os.File, row rowValueHolder, columns *[]columnFormat) {
	var txt string

	fmt.Fprintf(out, "| ")

	for i, v := range *columns {
		txt = row.value(v.num)

		if i > 0 {
			fmt.Fprintf(out, " | ")
		}
		fmt.Fprintf(out, "%s", presentation.StringFull(txt, v.size, " "))
	}
	fmt.Fprintf(out, " |\n")

}

// calculate the whole size in chars for the columns
func calcSize(columns *[]columnFormat) int {
	i := 1
	for _, v := range *columns {
		// size of the middle split
		i += 3
		// size of the content
		i += v.size
	}

	return i
}
