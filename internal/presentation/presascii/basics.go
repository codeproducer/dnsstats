package presascii

import (
	"bytes"
	"dnsstats/internal/parser"
	"dnsstats/internal/presentation"
	"fmt"
	"os"
	"sort"
	"strconv"
)

var maxObjToStringSize int

// print basic statistics of a report to console
func PrintBasicStats(out *os.File, report parser.DnsReport, preColumns PreColumnLayout) {
	// 20 is the indent where the text begins
	maxObjToStringSize = DefaultWidth + tableExtendWidth - 20

	var queries, blocked, cached = 0, 0, 0
	var queryTypes, clients = make(map[string]int), make(map[string]int)

	for _, v := range report.DnsEntries {
		queries += +v.Count

		// count different clients via a map
		putAndCount(queryTypes, v.QueryType, v.Count)

		// count different clients via a map
		putAndCount(clients, v.ClientIp, v.Count)

		// is blocked?
		if v.IsBlocked() {
			blocked += v.Count
		} else if v.IsCached() {
			cached += v.Count
		}
	}

	fmt.Fprintf(out, "Basic statistics:\n%s\n", presentation.StringFull("", DefaultWidth+tableExtendWidth, "="))
	fmt.Fprintf(out, "Creation time:      %s \n", report.CreationTime.Format(presentation.TimeFormat))
	fmt.Fprintf(out, "Files:              %s \n", buildFileList(report.Files))
	fmt.Fprintf(out, "Time:               %s - %s \n", report.TimeFrom.Format(presentation.TimeFormat), report.TimeTo.Format(presentation.TimeFormat))
	fmt.Fprintf(out, "Total queries:      %d \n", queries)
	fmt.Fprintf(out, "Queries blocked:    %d / %.1f%% \n", blocked, 100/float32(queries)*float32(blocked))
	fmt.Fprintf(out, "Queries cached:     %d / %.1f%% \n", cached, 100/float32(queries)*float32(cached))
	if preColumns.ClientIp {
		fmt.Fprintf(out, "Clients:            %d (%v)\n", len(clients), buildMapToTxtSortLimit(clients))
	}
	if preColumns.Typ {
		fmt.Fprintf(out, "Query types:        %d (%v)\n", len(queryTypes), buildMapToTxtSortLimit(queryTypes))
	}
}

func putAndCount(m map[string]int, k string, no int) {
	if c, ok := m[k]; ok {
		c = c + no
		m[k] = c
	} else {
		m[k] = no
	}
}

// build from a array a string
// limit it to a max size
func buildFileList(files []os.FileInfo) string {
	var b bytes.Buffer

	for i, f := range files {
		if i > 0 {
			b.WriteString(", ")
		}

		b.WriteString(f.Name())

		// break if max. length is exceeded
		if b.Len() > maxObjToStringSize {
			b.WriteString("@")
			break
		}
	}

	return b.String()
}

// build from a map a string
// order it by the value descending; limit it to a max size
func buildMapToTxtSortLimit(m map[string]int) string {
	// create a key array from the map-keys and sort it by
	keys := make([]string, 0, len(m))
	for key := range m {
		keys = append(keys, key)
	}
	sort.Slice(keys, func(i, j int) bool { return m[keys[i]] > m[keys[j]] })

	var b bytes.Buffer

	// iterate over the "sorted by value" keys and build string
	for i, k := range keys {
		if i > 0 {
			b.WriteString(", ")
		}

		b.WriteString(k + "[" + strconv.Itoa(m[k]) + "]")

		// break if max. length is exceeded
		if b.Len() > maxObjToStringSize+tableExtendWidth {
			b.WriteString("@")
			break
		}
	}

	return b.String()
}
