// +build !debug

package debug

import "log"
import "io/ioutil"

// disable logging via compiler
func Init() {
	// set logging format: "21:50:40 compressor.go:37: blablub..."
	log.SetFlags(0)
	log.SetOutput(ioutil.Discard)
}
