// +build debug

package debug

import "log"
import "os"

// if while build "-t=debug" is set => enable logging to stdout
func Init() {
	// set logging format: "21:50:40 compressor.go:37: blablub..."
	log.SetFlags(log.Ltime | log.Lshortfile)

	log.SetOutput(os.Stdout)

	log.Printf("debug is on\n")
}
