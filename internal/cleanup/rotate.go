package cleanup

import (
	"dnsstats/internal/config"
	"fmt"
	"log"
	"os"
	"path/filepath"
)

const TextPattern = config.FilePrefix + ".????????_??????" + config.FileSuffixText
const ChartPattern = config.FilePrefix + ".????????_??????" + config.FileSuffixChart

// walk to the files in the output-folder and remove "old" files
func Rotate(outFolder string, rotate int, filePattern string) {
	var names []string
	err := filepath.Walk(outFolder, func(path string, info os.FileInfo, err error) error {
		match, _ := filepath.Match(filePattern, info.Name())

		if !info.IsDir() && match {
			names = append(names, info.Name())
		}

		return nil
	})

	if err != nil {
		fmt.Printf("Error while walk through output-folder %s: %s \n", filePattern, err.Error())
		os.Exit(1)
	}

	if rotate < len(names) {
		for _, v := range names[:len(names)-rotate] {
			remove := filepath.Join(outFolder, v)
			log.Printf("rotate remove file %s \n", remove)
			os.Remove(remove)
		}
	}
}
