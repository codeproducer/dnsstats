package parser

import (
	"bufio"
	"fmt"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var dnsEntriesMap map[string]DnsEntry = make(map[string]DnsEntry)

var files []os.FileInfo
var timeFrom = time.Now()
var timeTo time.Time

// Jan 29 07:15:14 dnsmasq[15]: query[AAAA] googleads.g.doubleclick.net from 192.168.1.100
var rxDnsFile = regexp.MustCompile(`^\S{3} (\d| )\d \d\d:\d\d:\d\d dnsmasq\[\d+\]: .*$`)

// Feb  3 08:17:14 dnsmasq[11]: query[type=65] ocsp-a.g.aaplimg.com from 192.168.1.100
var rxQuery = regexp.MustCompile(`^.*query\[.*\].*$`)
var rxQueryGroup = regexp.MustCompile(`^.*query\[(.*)\] (.*) from (.*)$`)

// Feb  3 08:17:15 dnsmasq[11]: config firebaselogging-pa.googleapis.com is 0.0.0.0
// Feb  3 08:17:15 dnsmasq[11]: config firebaselogging-pa.googleapis.com is ::
// Feb  3 08:17:15 dnsmasq[11]: config firebaselogging-pa.googleapis.com is NODATA
var rxBlock = regexp.MustCompile(`^.*\: (/.*|config) (.*) is (0\.0\.0\.0.*|\:\:|NODATA.*)$`)

// Feb  3 09:35:59 dnsmasq[11]: cached outlook.ha.office365.com is <CNAME>
// Feb  3 11:42:02 dnsmasq[11]: cached geller-pa.googleapis.com is 2a00:1450:4001:829::200a
var rxCached = regexp.MustCompile(`^.*\: cached (.*) is .*$`)

// Feb  3 09:36:27 dnsmasq[11]: forwarded fonts.gstatic.com to 82.145.9.8
var rxForward = regexp.MustCompile(`^.*\: forwarded (.*) to .*$`)

// parameter struct for parsing the input files
type ParserParameter struct {
	Query QueryParameter
	Id    DnsEntryId
}

// reads the given (file/folder) name and extracts the informations
func getFileState(name string) (bool, bool, fs.FileInfo) {
	var isFolder, isRegex bool
	var isSingleFile fs.FileInfo

	// get file/folder stats to check if exists
	info, err := os.Stat(name)

	if os.IsNotExist(err) {
		// if file/folder not exists
		baseName := filepath.Base(name)
		// if a generic pattern, than it's a regex
		if strings.Contains(baseName, "*") || strings.Contains(baseName, "?") {
			isRegex = true
		} else {
			fmt.Println("The given file do not exists: " + err.Error())
			os.Exit(1)
		}
	} else if err == nil {
		// if exists, check if folder (for walkthrough) or a single (packed) file
		isFolder = info.IsDir()
		isSingleFile = info
	}

	return isFolder, isRegex, isSingleFile
}

// start parsing for a single file, GZ file or a folder
func Parse(name string, param ParserParameter, creationTime time.Time) DnsReport {
	isFolder, isRegex, fileInfo := getFileState(name)
	log.Printf("State of input %s isFolder=%v isRegex=%v fileInfo=%v", name, isFolder, isRegex, fileInfo)

	if isFolder {
		// name is a folder: go through the folder (with recursion)
		walkFolder(name, "", param)
	} else if isRegex {
		// name is a folder and a pattern: go through the folder (with recursion)
		dirName, baseName := filepath.Split(name)
		walkFolder(dirName, baseName, param)
	} else if fileInfo != nil {
		// single file
		potentielUnpack(name, fileInfo, param)
	}

	// create the final result report
	log.Printf("Created %d dnsEntries overall", len(dnsEntriesMap))
	return DnsReport{Files: files, TimeFrom: timeFrom, TimeTo: timeTo, DnsEntries: GetValuesFromMap(dnsEntriesMap), CreationTime: creationTime}
}

// walk to the files in the folder and there sub-folders
func walkFolder(dirName string, filePattern string, param ParserParameter) {
	err := filepath.Walk(dirName, func(path string, info os.FileInfo, err error) error {
		// check: no folder & if a filePattern is set and the current file is matched
		match, _ := filepath.Match(filePattern, info.Name())
		r := !info.IsDir() && (len(filePattern) == 0 || match)

		log.Printf("walk and filter path=%s with filePattern=%s to match=%v", path, filePattern, r)

		if r {
			potentielUnpack(path, info, param)
		}
		return nil
	})

	if err != nil {
		fmt.Printf("Error while walk through folder %s: %s \n", dirName, err.Error())
		os.Exit(1)
	}
}

// checks if the single file is a GZIP and must before unpacked
func potentielUnpack(originFile string, originFinfo os.FileInfo, param ParserParameter) {
	file := originFile
	var err error

	// check if gzipped
	if filepath.Ext(originFinfo.Name()) == ".gz" {
		// unpack it to the tmp folder
		target, err := unGzip(originFile, originFinfo)
		if err != nil {
			fmt.Printf("Error while unzipping %s: %s. Ignoring file and continue. \n", originFile, err.Error())
		} else {
			// parse the unpacked file and delete it after processing
			file = target
			defer removeTempFile(file)
			err = parseSingleFile(file, originFinfo, param)
		}
	} else {
		// single not packed file
		err = parseSingleFile(file, originFinfo, param)
	}

	if err != nil {
		fmt.Printf("Error while processing file %s: %s \n", originFile, err.Error())
		os.Exit(1)
	}
}

// parse a single unpacked file
// the file can be a temporarly unpacked file. the original name is present in the finfo
func parseSingleFile(fileName string, finfo os.FileInfo, param ParserParameter) error {
	log.Printf("Parsing file %s %s", fileName, finfo.Name())

	// do opening the file
	file, err := os.Open(fileName)
	if err != nil {
		return err
	}
	defer file.Close()

	// start the scanner for reading the file line-by-line
	scanner := bufio.NewScanner(file)

	// initiat some basics
	var entry DnsEntry
	var txt string
	count := 0 // only used for debugging purposes
	// initial is false and with the first found "query" the processing starts...
	init := false

	for scanner.Scan() {
		txt = scanner.Text()

		// check the first line if it's a dnsmasq logfile
		if !init {
			if rxDnsFile.MatchString(txt) {
				files = append(files, finfo)
			} else {
				fmt.Printf("File %s is no regular dnsmasq logfile; ignoring.\n", finfo.Name())
				return nil
			}
		}

		// create a new entry
		if rxQuery.MatchString(txt) {
			if init {
				add(&entry, &param, &finfo, &txt)
				count++
			}

			// update for every found entry the from-to timerange
			// the dnsmasq logs has no year, only month&date; use the year from the file
			timestamp := parseTime(txt, finfo.ModTime().Year())
			setReportTimerange(timestamp)

			parts := rxQueryGroup.FindStringSubmatch(txt)
			entry = DnsEntry{Timestamp: timestamp, QueryType: parts[1], Domain: parts[2], ClientIp: parts[3], Count: 1, State: DnsState(-1)}

			init = true
		} else if init {
			if rxBlock.MatchString(txt) {
				// blocked
				parts := rxBlock.FindStringSubmatch(txt)
				// set the state and the additional info (config or the file where the domain is configured)
				entry.State = Blocked
				entry.BlockInfo = parts[1]
			} else if rxCached.MatchString(txt) {
				// cached
				entry.State = Cached
			} else if rxForward.MatchString(txt) {
				// forwarded
				entry.State = Forward
			}
		}
	}

	// the last is complete
	addedId := add(&entry, &param, &finfo, &txt)
	if addedId != nil {
		// do some debug of the id
		log.Printf("Last added id: %s", *addedId)
	}
	count++

	if err := scanner.Err(); err != nil {
		return err
	}

	log.Printf("Parsed %d DNS entries from %s", count, fileName)

	return nil
}

// set the from-to timerange
func parseTime(line string, year int) time.Time {
	// line starts with the time "Jan 29 09:41:53 dnsmasq[15]: query[AAAA]..."
	// be aware in GO the numbers has a me,aning
	// 2006=year, 2=day, 15=hour and so on; see https://yourbasic.org/golang/format-parse-string-time-date-example/
	t, err := time.Parse("2006 Jan _2 15:04:05", strconv.Itoa(year)+" "+line[0:15])

	if err == nil {
		return t
	} else {
		return time.Now()
	}
}

// set the from-to timerange
func setReportTimerange(t time.Time) {
	if t.Before(timeFrom) {
		timeFrom = t
	}
	if t.After(timeTo) {
		timeTo = t
	}
}

// add or update (the counting) to the result map where potentiell query passed
func add(e *DnsEntry, param *ParserParameter, finfo *os.FileInfo, txt *string) *string {
	if !isEntryAllowed(&param.Query, e) {
		return nil
	}

	if !e.isComplete() {
		log.Printf("File %s contains uncomplete query >%s< near line >%s<.\n", (*finfo).Name(), e, *txt)
		return nil
	} else {
		return Add(dnsEntriesMap, e, param.Id)
	}
}
