package parser

import (
	"fmt"
	"os"
	"time"
)

// represents a report with meta-date and the "aggregated" DnsEntry's
type DnsReport struct {
	Files        []os.FileInfo
	CreationTime time.Time
	TimeFrom     time.Time
	TimeTo       time.Time
	DnsEntries   []DnsEntry
}

func (e DnsReport) HasEntries() bool {
	return len(e.DnsEntries) > 0
}

func (e DnsReport) CopyReport(newEntries []DnsEntry) DnsReport {
	return DnsReport{Files: e.Files, TimeFrom: e.TimeFrom, TimeTo: e.TimeTo, DnsEntries: newEntries, CreationTime: e.CreationTime}
}

// enum for Dns State with ToString and ToDnsState(string)
type DnsState int

const (
	Blocked DnsState = iota
	Cached
	Forward
)

var dnsStateStr = [...]string{"blocked", "cached", "forward"}

func (s DnsState) ToString() string {
	return dnsStateStr[s]
}

func ToDnsState(str string) (ok bool, state DnsState) {
	if str == dnsStateStr[0] {
		return true, Blocked
	} else if str == dnsStateStr[1] {
		return true, Cached
	} else if str == dnsStateStr[2] {
		return true, Forward
	} else {
		// return a ok=false
		return false, DnsState(-1)
	}
}

// a DnsEntry is a processed DNS request, its caller, state and some more infos
type DnsEntry struct {
	Timestamp time.Time // timestamp; if compressed one of a aggregated timestamp
	QueryType string
	Domain    string
	ClientIp  string

	Count     int      // count of quiries
	State     DnsState // cached, blocked...
	BlockInfo string   // only filled when blocked

	Id string // the id if compressed via compressor (example "<time>_A_www.g.com")
}

func (e DnsEntry) isComplete() bool {
	return e.QueryType != "" && e.Domain != "" && e.ClientIp != "" && e.State >= 0
}

func (e DnsEntry) IsBlocked() bool {
	return e.State == Blocked
}

func (e DnsEntry) IsCached() bool {
	return e.State == Cached
}

func (e *DnsEntry) increase(v int) {
	e.Count += v
}

func (e DnsEntry) String() string {
	return fmt.Sprintf("Timestamp=%s, QueryType=%s, Domain=%s, ClientIp=%s, State=%d Count=%d BlockInfo=%s",
		e.Timestamp, e.QueryType, e.Domain, e.ClientIp, e.State, e.Count, e.BlockInfo)
}

func (e DnsEntry) Hour() string {
	// HH (only hour, no minutes!)
	// see https://yourbasic.org/golang/format-parse-string-time-date-example/ for formating-number-rules
	return e.Timestamp.Format("15")
}
