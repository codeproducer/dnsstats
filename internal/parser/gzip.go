package parser

import (
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

const appTmpDir = "/dnsstats"

// flag for unzip caching
// false: always unzip file and use temporarly filenames
// true: to reuse unzipped files (=true)
var cacheUnzipFiles = false

// os dependend temp dir (linux: /tmp)
var tmpDir string

// initialize one time
func InitGzip(cacheUnzip bool) {
	cacheUnzipFiles = cacheUnzip

	// check if app-tmp-folder exists
	_, err := os.Stat(os.TempDir() + appTmpDir)
	appTmpExists := !os.IsNotExist(err)

	if cacheUnzipFiles {
		// if we are in unzip-cache mode, create the dnsstats folder under /tmp
		tmpDir = os.TempDir() + appTmpDir

		if !appTmpExists {
			log.Printf("Creating temp-folder %s ...", tmpDir)
			err := os.MkdirAll(tmpDir, 0700)
			if err != nil {
				fmt.Printf("Can not create temp dir %s!", tmpDir)
				os.Exit(1)
			}
		}

		// add file separator for further use
		tmpDir = tmpDir + string(os.PathSeparator)
	} else {
		// we use the default /tmp and place the unzipped directly / without subfolder
		tmpDir = os.TempDir()

		// we also remove all existing temp files from this app
		if appTmpExists {
			log.Printf("Remove temp-folder %s ...", os.TempDir()+appTmpDir)
			os.RemoveAll(os.TempDir() + appTmpDir)
		}
	}
}

// (unpacks the source file | check if already exists) and returns it's path
func unGzip(source string, finfo os.FileInfo) (string, error) {
	if cacheUnzipFiles {
		// if we are in cached mode, a already unzipped file must not be unzipped again
		// check the timestamp of the original-GZ-file and a potential unpacked
		unpackedFileName := tmpDir + fileNameWithoutExtTrimSuffix(finfo.Name())
		unpackedInfo, err := os.Stat(unpackedFileName)

		if err == nil && finfo.ModTime().Equal(unpackedInfo.ModTime()) {
			// file exists and date is equal, so use it again
			log.Printf("File %s already unpacked; use it again", source)
			return unpackedFileName, nil
		} else {
			// unpacked file do not exists or has no equal date: unzip it and set date & permission
			resultFilename, err := unGzipFile(source, finfo)
			if err == nil {
				err = os.Chtimes(resultFilename, finfo.ModTime(), finfo.ModTime())
				err = os.Chmod(resultFilename, 0600)
			}
			return resultFilename, err
		}
	} else {
		return unGzipFile(source, finfo)
	}
}

// unpacks the source file to a temporary file under "/tmp/xxx" and returns it's path
// gz can not contain more then one file or folders so no zip-slipping can occur
// if we in the cached-mode the filename is not "random" because we must find it the next call ;-)
func unGzipFile(source string, finfo os.FileInfo) (string, error) {
	file, err := os.Open(source)
	if err != nil {
		return "", err
	}
	defer file.Close()

	reader, err := gzip.NewReader(file)
	if err != nil {
		return "", err
	}
	defer reader.Close()

	var writer *os.File
	if !cacheUnzipFiles {
		// create a file under /tmp/dnsstats.486249012.tmplog
		writer, err = ioutil.TempFile(tmpDir, "dnsstats.*.tmplog")
	} else {
		// create a file under /tmp/dnsstats/<original name>
		writer, err = os.Create(tmpDir + fileNameWithoutExtTrimSuffix(finfo.Name()))
	}
	if err != nil {
		return "", err
	}
	defer writer.Close()

	log.Printf("Unpack %s -> %s", source, writer.Name())

	_, err = io.Copy(writer, reader)
	if err != nil {
		return "", err
	} else {
		return writer.Name(), nil
	}
}

// removes a temp UNzipped created file
func removeTempFile(fileName string) {
	if !cacheUnzipFiles {
		log.Printf("Remove tmp file %s", fileName)
		os.Remove(fileName)
	}
}

// removes the suffix (.gz) from the filename
func fileNameWithoutExtTrimSuffix(fileName string) string {
	return strings.TrimSuffix(fileName, filepath.Ext(fileName))
}
