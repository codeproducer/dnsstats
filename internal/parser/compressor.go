package parser

import (
	"fmt"
	"log"
)

const Deli = '_'

// calc the id of an DnsEntry
// the id is the the "string" where the "same" queries are aggregated/compressed
type DnsEntryId func(*DnsEntry) string

// ################################ Parser IDs

// id for "normal"/first aggregate case/level - the `sl`/long format
func TimeTypeDomainClientStateId(e *DnsEntry) string {
	// YYYY-MM-DD HH (only hour, no minutes!)
	// see https://yourbasic.org/golang/format-parse-string-time-date-example/ for formating-number-rules
	t := e.Timestamp.Format("2006-01-02 15")

	return fmt.Sprintf("%s%c%s%c%s%c%s%c%d", t, Deli, e.QueryType, Deli, e.Domain, Deli, e.ClientIp, Deli, e.State)
}

// id for "normal"/first aggregate case/level - only "domain and state"
func TimeDomainStateId(e *DnsEntry) string {
	// YYYY-MM-DD HH (only hour, no minutes!)
	// see https://yourbasic.org/golang/format-parse-string-time-date-example/ for formating-number-rules
	t := e.Timestamp.Format("2006-01-02 15")

	return fmt.Sprintf("%s%c%s%c%d", t, Deli, e.Domain, Deli, e.State)
}

// ################################ Compressor IDs for top-tables

// id for "top domains" case - the `sl`/long format
func TypeDomainClientStateId(e *DnsEntry) string {
	return fmt.Sprintf("%s%c%s%c%s%c%d", e.QueryType, Deli, e.Domain, Deli, e.ClientIp, Deli, e.State)
}

// id for "top domains" case - only "domain"
func DomainId(e *DnsEntry) string {
	return fmt.Sprintf("%s", e.Domain)
}

// id for "time based" & state case
func HourStateId(e *DnsEntry) string {
	return fmt.Sprintf("%s%c%d", e.Hour(), Deli, e.State)
}

// ################################ Compressor IDs for timed-tables

// id for "time based" & domain case
func HourDomainId(e *DnsEntry) string {
	return fmt.Sprintf("%s%c%s", e.Hour(), Deli, e.Domain)
}

// ################################ Compressor IDs for graphical-tables

// id for "normal"/first aggregate case/level - only "domain and state"
func DateHourStateId(e *DnsEntry) string {
	// YYYY-MM-DD HH (only hour, no minutes!)
	// see https://yourbasic.org/golang/format-parse-string-time-date-example/ for formating-number-rules
	t := e.Timestamp.Format("2006-01-02 15")

	return fmt.Sprintf("%s%c%d", t, Deli, e.State)
}

// add or update an entry in the result map
// if the same entry with the DnsEntryId exists the Count will be updated
// returns the id of the added entry
func Add(dnsEntriesMap map[string]DnsEntry, e *DnsEntry, entryId DnsEntryId) *string {
	id := entryId(e)
	if v, ok := dnsEntriesMap[id]; ok {
		v.increase(e.Count)
		dnsEntriesMap[id] = v
	} else {
		dnsEntriesMap[id] = *e
	}
	return &id
}

// returns the values as a array from the maps values
func GetValuesFromMap(dnsEntries map[string]DnsEntry) []DnsEntry {
	result := []DnsEntry{}

	for k, v := range dnsEntries {
		v.Id = k
		result = append(result, v)
	}

	return result
}

// compress the sourceEntries with the DnsEntryId to a new array
// same Id's will increases the Count
func Compress(sourceEntries []DnsEntry, entryId DnsEntryId) []DnsEntry {
	resultMap := make(map[string]DnsEntry)
	var id *string

	for _, v := range sourceEntries {
		id = Add(resultMap, &v, entryId)
	}

	log.Printf("compress from %d mapEntries to %d. last id: %s", len(sourceEntries), len(resultMap), *id)

	return GetValuesFromMap(resultMap)
}
