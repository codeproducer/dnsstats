package parser

import "log"
import "regexp"

// a query of an attribute has the query and the invert/negative flag
type query struct {
	isSet  bool
	value  string
	invert bool
}

// struct for all supported queries
type QueryParameter struct {
	domainQuery query
	clientIp    query
	qtype       query
}

// creates from the several command-line parameters a struct object
func MakeParameter(domain string, domainV bool,
	clientIp string, clientIpV bool,
	qtype string, qtypeV bool) QueryParameter {
	var qp QueryParameter

	if domain != "" {
		qp.domainQuery = query{true, domain, domainV}
		log.Printf("domain query is set to %#v", qp.domainQuery)
	}

	if clientIp != "" {
		qp.clientIp = query{true, clientIp, clientIpV}
		log.Printf("clientIp query is set to %#v", qp.clientIp)
	}

	if qtype != "" {
		qp.qtype = query{true, qtype, qtypeV}
		log.Printf("query-type query is set to %#v", qp.qtype)
	}

	return qp
}

// checks the DnsEntry against the query definition
func isEntryAllowed(query *QueryParameter, e *DnsEntry) bool {
	if query.domainQuery.isSet {
		if ok, _ := regexp.MatchString(query.domainQuery.value, e.Domain); ok == query.domainQuery.invert {
			return false
		}
	}

	if query.clientIp.isSet {
		if ok, _ := regexp.MatchString(query.clientIp.value, e.ClientIp); ok == query.clientIp.invert {
			return false
		}
	}

	if query.qtype.isSet {
		if ok, _ := regexp.MatchString(query.qtype.value, e.QueryType); ok == query.qtype.invert {
			return false
		}
	}

	return true
}
